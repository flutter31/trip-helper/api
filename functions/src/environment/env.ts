import { CoreModels } from '@core/models';

export const environment: CoreModels.IEnv = {
  apiKey: 'AIzaSyAV4uLBTrRCK2c4XRMLXeSMqPykeFYJ6-k',
  googleapisAuthEndpoint: 'https://identitytoolkit.googleapis.com/v1/',
  googleapisSecureEndpoint: 'https://securetoken.googleapis.com/v1/',
};
