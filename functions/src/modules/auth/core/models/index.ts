export namespace AuthModels {
  export namespace SignIn {
    export interface IRequestBody {
      readonly email: string;
      readonly password: string;
    }

    export interface IDto {
      readonly email: string;
      readonly password: string;
      readonly returnSecureToken: true;
    }

    export interface IResponse {
      readonly token: string;
    }
  }

  export namespace SignUp {
    export interface IRequestBody {
      readonly email: string;
      readonly password: string;
    }

    export interface IDto {
      readonly email: string;
      readonly password: string;
      readonly returnSecureToken: true;
    }

    export interface IResponse {
      readonly token: string;
    }
  }

  export namespace RefreshToken {
    export interface IRequestBody {
      readonly refreshToken: string;
    }

    export interface IDto {
      readonly grant_type: 'refresh_token';
      readonly refresh_token: string;
    }
  }
}
