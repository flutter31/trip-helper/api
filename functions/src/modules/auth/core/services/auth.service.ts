import fetch, { Response } from 'node-fetch';

import { environment } from '@env/env';

import { AuthModels } from '../models';

export class AuthService {
  private static readonly routes = {
    signUp: `${environment.googleapisAuthEndpoint}accounts:signUp?key=${environment.apiKey}`,
    signIn: `${environment.googleapisAuthEndpoint}accounts:signInWithPassword?key=${environment.apiKey}`,
    refreshToken: `${environment.googleapisSecureEndpoint}token?key=${environment.apiKey}`,
  };

  public static async signUp(dto: AuthModels.SignUp.IDto): Promise<Response> {
    const body: string = JSON.stringify(dto);

    return fetch(this.routes.signUp, {
      method: 'POST',
      body,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  public static async signIn(dto: AuthModels.SignIn.IDto): Promise<Response> {
    const body: string = JSON.stringify(dto);

    return fetch(this.routes.signIn, {
      method: 'POST',
      body,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }

  public static async refreshToken(
    dto: AuthModels.RefreshToken.IDto
  ): Promise<Response> {
    const body: string = `grant_type=${dto.grant_type}&refresh_token=${dto.refresh_token}`;

    return fetch(this.routes.refreshToken, {
      method: 'POST',
      body,
      headers: {
        'Content-Type': 'Content-Type: application/x-www-form-urlencoded',
      },
    });
  }
}
