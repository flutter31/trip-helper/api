import * as functions from 'firebase-functions';

import { signUpController } from './controllers/sign-up.controller';
import { signInController } from './controllers/sign-in.controller';
import { refreshTokenController } from './controllers/refresh-token.controller';

export const signIn = functions.https.onRequest(async (request, response) => {
  switch (request.method) {
    case 'POST':
      await signInController(request, response);
      break;
    default:
      response
        .status(404)
        .send(
          `Route "signIn" is not set for ${request.method} HTTP request method!`
        );
  }
});

export const signUp = functions.https.onRequest(async (request, response) => {
  switch (request.method) {
    case 'POST':
      await signUpController(request, response);
      break;
    default:
      response
        .status(404)
        .send(
          `Route "signUp" is not set for ${request.method} HTTP request method!`
        );
  }
});

export const refreshToken = functions.https.onRequest(
  async (request, response) => {
    switch (request.method) {
      case 'POST':
        await refreshTokenController(request, response);
        break;
      default:
        response
          .status(404)
          .send(
            `Route "refreshToken" is not set for ${request.method} HTTP request method!`
          );
    }
  }
);
