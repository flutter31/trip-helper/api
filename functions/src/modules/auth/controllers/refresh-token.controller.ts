import * as express from 'express';
import { Response } from 'node-fetch';

import { CoreModels } from '@core/models';
import { ErrorService } from '@core/services/error.service';

import { AuthModels } from '../core/models';
import { AuthService } from '../core/services/auth.service';

export const refreshTokenController = async (
  request: CoreModels.IRequest,
  response: express.Response
) => {
  const reqBody: AuthModels.RefreshToken.IRequestBody = request.body;

  if (!reqBody) {
    ErrorService.send(response, 400, 'Input data is not provided!');
    return;
  }

  const dto: AuthModels.RefreshToken.IDto = {
    grant_type: 'refresh_token',
    refresh_token: reqBody.refreshToken,
  };

  let res: Response | undefined;
  let resBody: any;

  try {
    res = await AuthService.refreshToken(dto);
    resBody = await res?.json();
  } catch (error) {
    ErrorService.send(response, 500, error.message);
  }

  if (!res?.ok) {
    const { error }: CoreModels.IGoogleapiError = resBody;

    ErrorService.send(response, error.code, error.message);
    return;
  }

  response.send({
    data: resBody,
  });
};
