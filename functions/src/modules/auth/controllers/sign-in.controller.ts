import * as express from 'express';
import { Response } from 'node-fetch';

import { CoreModels } from '@core/models';
import { ErrorService } from '@core/services/error.service';

import { AuthModels } from '../core/models';
import { AuthService } from '../core/services/auth.service';

export const signInController = async (
  request: CoreModels.IRequest,
  response: express.Response
) => {
  const reqBody: AuthModels.SignIn.IRequestBody = request.body;

  if (!reqBody) {
    ErrorService.send(response, 400, 'Input data is not provided!');
    return;
  }

  const dto: AuthModels.SignIn.IDto = {
    email: reqBody.email,
    password: reqBody.password,
    returnSecureToken: true,
  };

  let res: Response | undefined;
  let resBody: any;

  try {
    res = await AuthService.signIn(dto);
    resBody = await res?.json();
  } catch (error) {
    ErrorService.send(response, 500, error.message);
  }

  if (!res?.ok) {
    const { error }: CoreModels.IGoogleapiError = resBody;

    ErrorService.send(response, error.code, error.message);
    return;
  }

  response.send({
    data: resBody,
  });
};
