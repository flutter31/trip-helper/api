import * as express from 'express';

export class ErrorService {
  public static send(
    response: express.Response,
    code: number,
    message: string
  ) {
    response.status(code).send({ code, message });
  }
}
