import * as express from 'express';

export namespace CoreModels {
  export interface IRequest extends express.Request {
    rawBody: Buffer;
  }

  export class RequestError extends Error {
    constructor(public readonly message: string) {
      super(message);
    }
  }

  export class ResponseError extends Error {
    constructor(public readonly error: IResponseError) {
      super(error.statusText);
    }
  }

  export interface IResponseError {
    readonly status: number;
    readonly statusText: string;
    readonly body: any;
  }

  export interface IEnv {
    readonly apiKey: string;
    readonly googleapisAuthEndpoint: string;
    readonly googleapisSecureEndpoint: string;
  }

  export interface IGoogleapiError {
    error: {
      code: number;
      message: string;
      errors: {
        message: string;
        domain: string;
        reason: string;
      }[];
    };
  }
}
