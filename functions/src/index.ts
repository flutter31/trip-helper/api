import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import 'module-alias/register';

admin.initializeApp(functions.config().firebase);

export * from '@auth/auth';
